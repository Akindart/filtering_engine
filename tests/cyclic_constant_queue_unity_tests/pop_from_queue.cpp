/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/utilities/assertion_machinery.hpp"
#include <iostream>

struct info_type {
    int a;
    int b;

    bool operator==(info_type const &info) const
    {
        return a == info.a && b == info.b;
    }

    friend inline std::ostream &operator<<(std::ostream &os, info_type &info)
    {
        os << "[info_type a: " << info.a << " b: " << info.b << "]";
        return os;
    }
};

int main()
{
    data_structures::cyclic_constant_size_queue<info_type, 4> queue;

    info_type a{ 1, 1 };
    info_type b{ 2, 2 };

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(a);
    ASSERT_FE(queue.back() == a, "");

    info_type poped_item = queue.pop();
    std::cout << "Queue pop: " << queue << std::endl;

    ASSERT_FE((poped_item == a && queue.empty()), "");

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(a);
    ASSERT_FE(queue.back() == a, "");

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(b);
    ASSERT_FE(queue.back() == b, "");

    std::cout << "Queue second insertion: " << queue << std::endl;

    poped_item = queue.pop();
    std::cout << "Queue pop: " << queue << std::endl;

    ASSERT_FE((poped_item == a && queue.front() == b), "");

    return 0;
}