/*!
*
* @author spades
* @date 09/06/23
*
*/

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>
#include <memory>

struct info_type {
    int a;
    int b;

    info_type(int a, int b)
        : a(a)
        , b(b){};

    friend bool operator==(info_type const &lhs, info_type const &rhs)
    {
        return rhs.a == lhs.a && rhs.b == lhs.b;
    }
    bool operator==(info_type &&rhs) const
    {
        return rhs.a == a && rhs.b == b;
    }

    friend inline std::ostream &operator<<(std::ostream &os, info_type &info)
    {
        os << "[info_type a: " << info.a << " b: " << info.b << "]";
        return os;
    }
};

struct info_type2 {
    int a;
    int b;

    info_type2(int a, int b)
        : a(a)
        , b(b){};

    friend bool operator==(info_type2 const &lhs, info_type2 const &rhs)
    {
        return rhs.a == lhs.a && rhs.b == lhs.b;
    }

    friend inline std::ostream &operator<<(std::ostream &os, info_type2 &info)
    {
        os << "[info_type a: " << info.a << " b: " << info.b << "]";
        return os;
    }
};

int main()
{
    using qt = data_structures::cyclic_constant_size_queue<
        std::variant<std::unique_ptr<info_type>, std::unique_ptr<info_type2>>,
        4>;

    qt queue;

    ASSERT_FE((std::convertible_to<std::unique_ptr<info_type2>,
                                   std::variant<std::unique_ptr<info_type>,
                                                std::unique_ptr<info_type2>>>),
              "");

    ASSERT_FE(utilities::fifo_like_container<qt>, "");
    ASSERT_FE(utilities::has_back_insert_method<qt>, "");
    ASSERT_FE(utilities::fifo_like_remove<qt>, "");
    ASSERT_FE(utilities::can_watch_size<qt>, "");

    std::unique_ptr<info_type> a = std::make_unique<info_type>(1, 1);
    std::unique_ptr<info_type2> b = std::make_unique<info_type2>(2, 2);
    std::unique_ptr<info_type> d = std::make_unique<info_type>(4, 4);

    std::cout << "Queue front insertion: " << std::endl;
    queue.push(std::forward<std::unique_ptr<info_type>>(a));
    std::unique_ptr<info_type> test_a = std::make_unique<info_type>(1, 1);
    ASSERT_FE((*std::get<std::unique_ptr<info_type>>(queue.front()) == *test_a),
              "");

    std::cout << "Queue front insertion: " << std::endl;
    queue.push(std::forward<std::unique_ptr<info_type2>>(b));
    std::unique_ptr<info_type2> test_b = std::make_unique<info_type2>(2, 2);
    ASSERT_FE((*std::get<std::unique_ptr<info_type2>>(queue.back()) == *test_b),
              "");

    std::cout << "Queue front insertion: " << std::endl;
    queue.push(std::make_unique<info_type2>(3, 3));
    auto test_c = std::make_unique<info_type2>(3, 3);
    ASSERT_FE((*std::get<std::unique_ptr<info_type2>>(queue.back()) == *test_c),
              "");

    std::cout << "Queue front insertion: " << std::endl;
    queue.push(std::forward<std::unique_ptr<info_type>>(d));
    std::unique_ptr<info_type> test_d = std::make_unique<info_type>(4, 4);
    ASSERT_FE((*std::get<std::unique_ptr<info_type>>(queue.back()) == *test_d),
              "");

    return 0;
}