/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/utilities/assertion_machinery.hpp"
#include <iostream>

struct info_type {
    int a;
    int b;

    bool operator==(info_type const &info) const
    {
        return a == info.a && b == info.b;
    }

    bool operator==(info_type const &&info) const
    {
        return a == info.a && b == info.b;
    }

    friend inline std::ostream &operator<<(std::ostream &os, info_type &info)
    {
        os << "[info_type a: " << info.a << " b: " << info.b << "]";
        return os;
    }
};

int main()
{
    data_structures::cyclic_constant_size_queue<info_type, 4> queue;

    info_type a{ 1, 1 };
    info_type b{ 2, 2 };
    info_type d{ 4, 4 };

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(a);
    ASSERT_FE(queue.back() == a, "");

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(b);
    ASSERT_FE(queue.back() == b, "");

    std::cout << "Queue second insertion: " << queue << std::endl;
    queue.push(info_type{ 3, 3 });
    ASSERT_FE((queue.back() == info_type{ 3, 3 }), "");

    std::cout << "Queue third insertion: " << queue << std::endl;
    queue.push(d);
    ASSERT_FE(queue.back() == d, "");

    std::cout << "Queue fourth insertion: " << queue << std::endl;

    int result;

    try {

        auto init_size = queue.size() + 1;

        for(std::size_t i=0; i<init_size; ++i){
            info_type poped_elem = queue.pop();
            std::cout << "poped_item: " << poped_elem << std::endl;
            std::cout << "queue size " << queue.size() << std::endl;
            std::cout << "Queue pop: " << queue << std::endl;
        }
        result = 0;

    } catch (std::invalid_argument &e) {
        std::cout << "Exception: " << e.what() << std::endl;
        result = 1;
    }

    return result;
}