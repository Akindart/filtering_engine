/*!
*
* @author spades
* @date 25/02/23
*
*/

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>

struct info_type {
    int a;
    int b;

    bool operator==(info_type &rhs) const
    {
        return rhs.a == a && rhs.b == b;
    }
    bool operator==(info_type &&rhs) const
    {
        return rhs.a == a && rhs.b == b;
    }

    friend inline std::ostream &operator<<(std::ostream &os, info_type &info)
    {
        os << "[info_type a: " << info.a << " b: " << info.b << "]";
        return os;
    }
};

int main()
{
    data_structures::cyclic_constant_size_queue<info_type, 4> queue;

    info_type a{ 1, 1 };
    info_type b{ 2, 2 };
    info_type d{ 4, 4 };

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(a);
    ASSERT_FE(queue.back() == a, "");

    std::cout << "Queue front insertion: " << queue << std::endl;
    queue.push(b);
    ASSERT_FE(queue.back() == b, "");

    std::cout << "Queue second insertion: " << queue << std::endl;
    queue.push(info_type{ 3, 3 });
    ASSERT_FE((queue.back() == info_type{ 3, 3 }), "");

    std::cout << "Queue third insertion: " << queue << std::endl;
    queue.push(d);
    ASSERT_FE(queue.back() == d, "");

    std::cout << "Queue fourth insertion: " << queue << std::endl;

    return 0;
}