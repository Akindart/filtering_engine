/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include <iostream>

struct info_type {
    int a;
    int b;

    bool operator==(info_type const &info) const
    {
        return a == info.a && b == info.b;
    }

    friend inline std::ostream &operator<<(std::ostream &os, info_type &info)
    {
        os << "[info_type a: " << info.a << " b: " << info.b << "]";
        return os;
    }
};

int main()
{
    data_structures::cyclic_constant_size_queue<info_type, 4> queue;

    int result;

    try {
        info_type poped_item = queue.pop();
        std::cout << "Queue pop: " << queue << std::endl;
        result = 0;

    } catch (std::invalid_argument &e) {
        std::cout << "Exception: " << e.what() << std::endl;
        result = 1;
    }

    return result;
}