/*!
*
* @author user
* @date 30/04/23
*
*/

#include "include/utilities/command.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <string>
#include <iostream>

struct get_sum {
    int operator()(int a, int b)
    {
        return a + b;
    }
};

struct get_const {
    int operator()()
    {
        return 17;
    }
};

struct c1 {
    int operator()(int &&) const
    {
        return 0;
    }
};

struct c2 {
    std::string operator()(std::string &&str)
    {
        return { str };
    }
};

struct c3 {
    std::string operator()(std::string &&str, int &i)
    {
        std::stringstream ss;

        ss << str << " " << i;

        return ss.str();
    }
};

int main()
{
    using namespace utilities;

    auto cmd_fcn_1 = command_factory::create_unique_cmd(c1(), 1);
    ASSERT_FE((cmd_fcn_1->execute() == 0), "Check for correct return value.");

    std::string str_test{ "Returning string" };
    auto cmd_fcn_2 = command_factory::create_unique_cmd(c2(), str_test);
    ASSERT_FE((cmd_fcn_2->execute() == str_test),
              "Check for correct return value.");

    auto cmd_fcn_3 = command_factory::create_unique_cmd(
        c3(), std::string{ "test string" }, 17);
    ASSERT_FE((cmd_fcn_3->execute() == "test string 17"),
              "Check for correct return value.");

    auto cmd_int_sum = command_factory::create_unique_cmd(get_sum(), 10, 20);
    int sum_result = cmd_int_sum->execute();
    ASSERT_FE((cmd_int_sum->execute() == 30),
              "Check for correct return value.");

    auto cmd_get_17 = command_factory::create_unique_cmd(get_const());
    ASSERT_FE((sum_result + cmd_get_17->execute() == 47),
              "Check for correct return and operation value.");

    return 0;
};