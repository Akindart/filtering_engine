/*!
*
* @author spades
* @date 15/03/23
*
*/

#include "include/utilities/command.hpp"
#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <sstream>

void test_callable(int a, float b, bool c)
{
}

int main()
{
    using func_type =
        decltype(utilities::get_callable_type::type(&test_callable));

    auto cmd = utilities::command_factory::create_unique_cmd(&test_callable, 10,
                                                             (float)20, false);

    std::stringstream ss;

    ss << *cmd;

    ASSERT_FE(ss.str() == "[ Command [ Parameters [ 10 20 0 ] ] ]", "");

    return 0;
}