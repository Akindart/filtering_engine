/*!
*
* @author spades
* @date 01/03/23
*
*/

#include "include/type_utilities/type_utilities_base.hpp"
#include "include/utilities/command.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <concepts>
#include <sstream>
#include <string>
#include <iostream>
#include <tuple>

int sum1(int a, int b)
{
    return a + b;
}

int get_17()
{
    return 17;
}

int function_1(int &&)
{
    return 0;
}

std::string function_2(std::string &&str)
{
    return { str };
}

std::string function_3(std::string &&str, int &i)
{
    std::stringstream ss;

    ss << str << " " << i;

    return ss.str();
}

std::string function_4(std::string *str)
{
    return { *str };
}

int main()
{
    using namespace utilities;

    auto cmd_fcn_1 = command_factory::create_unique_cmd(function_1, 1);
    ASSERT_FE((cmd_fcn_1->execute() == 0), "Check for correct return value.");

    std::string str_test{ "Returning string" };
    auto cmd_fcn_2 = command_factory::create_unique_cmd(function_2, str_test);
    ASSERT_FE((cmd_fcn_2->execute() == str_test),
              "Check for correct return value.");

    auto cmd_fcn_4 = command_factory::create_unique_cmd(function_4, &str_test);
    ASSERT_FE((cmd_fcn_4->execute() == str_test),
              "Check for correct return value.");

    int a = 17;
    auto cmd_fcn_3 = command_factory::create_unique_cmd(
        function_3, std::string{ "test string" }, a);
    ASSERT_FE((cmd_fcn_3->execute() == "test string 17"),
              "Check for correct return value.");

    auto cmd_int_sum = command_factory::create_unique_cmd(&sum1, 10, 20);
    int sum_result = cmd_int_sum->execute();
    ASSERT_FE((cmd_int_sum->execute() == 30),
              "Check for correct return value.");

    auto cmd_get_17 = command_factory::create_unique_cmd(&get_17);
    ASSERT_FE((sum_result + cmd_get_17->execute() == 47),
              "Check for correct return and operation value.");

    return 0;
};