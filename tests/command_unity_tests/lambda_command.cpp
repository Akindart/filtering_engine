/*!
*
* @author spades
* @date 12/05/23
*
*/

#include "include/utilities/command.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <string>
#include <iostream>

int main()
{
    using namespace utilities;

    auto cmd_fcn_1 =
        command_factory::create_unique_cmd([](int &&) -> int { return 0; }, 1);
    ASSERT_FE((cmd_fcn_1->execute() == 0), "Check for correct return value.");

    std::string str_test{ "Returning string" };
    auto cmd_fcn_2 = command_factory::create_unique_cmd(
        [](std::string &&str) -> std::string { return { str }; }, str_test);
    ASSERT_FE((cmd_fcn_2->execute() == str_test),
              "Check for correct return value.");

    auto cmd_fcn_3 = command_factory::create_unique_cmd(
        [](std::string &&str, int &i) -> std::string {
            std::stringstream ss;

            ss << str << " " << i;

            return ss.str();
        },
        std::string{ "test string" }, 17);
    ASSERT_FE((cmd_fcn_3->execute() == "test string 17"),
              "Check for correct return value.");

    auto cmd_int_sum = command_factory::create_unique_cmd(
        [](int a, int b) -> int { return a + b; }, 10, 20);
    int sum_result = cmd_int_sum->execute();
    ASSERT_FE((cmd_int_sum->execute() == 30),
              "Check for correct return value.");

    auto cmd_get_17 =
        command_factory::create_unique_cmd([]() -> int { return 17; });
    ASSERT_FE((sum_result + cmd_get_17->execute() == 47),
              "Check for correct return and operation value.");

    return 0;
};