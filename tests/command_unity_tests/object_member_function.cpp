/*!
*
* @author spades
* @date 01/03/23
*
*/

#include "include/utilities/command.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <string>
#include <iostream>

struct get_sum {
    int sum1(int a, int b)
    {
        return a + b;
    }
};

struct get_const {
    int get_17()
    {
        return 17;
    }
};

struct c1 {
    int function_1(int &&)
    {
        return 0;
    }
    template <typename T>
    int execute(T &&)
    {
        return 0;
    }
};

struct c2 {
    std::string function_2(std::string &&str)
    {
        return { str };
    }
};

struct c3 {
    std::string function_3(std::string &&str, int &i)
    {
        std::stringstream ss;

        ss << str << " " << i;

        return ss.str();
    }
};

int main()
{
    using namespace utilities;

    c1 c1_obj;
    std::reference_wrapper<c1> c1_ref_obj{ c1_obj };
    auto cmd_fcn_1 =
        command_factory::create_unique_cmd(&c1::function_1, c1_ref_obj, 1);
    ASSERT_FE((cmd_fcn_1->execute() == 0), "Check for correct return value.");

    auto cmd_fcn_1_template =
        command_factory::create_unique_cmd(&c1::execute<int &>, c1_ref_obj, 1);
    ASSERT_FE((std::is_same_v<decltype(cmd_fcn_1_template),
                              decltype(command_factory::create_unique_cmd(
                                  &c1::execute<int &>, c1_ref_obj, 1))>),
              "Check for command type for template.");
    ASSERT_FE((cmd_fcn_1_template->execute() == 0),
              "Check for correct return value.");

    c2 c2_obj;
    std::string str_test{ "Returning string" };
    auto cmd_fcn_2 =
        command_factory::create_unique_cmd(&c2::function_2, c2_obj, str_test);
    ASSERT_FE((cmd_fcn_2->execute() == str_test),
              "Check for correct return value.");

    c3 c3_obj;
    int a = 17;
    auto cmd_fcn_3 = command_factory::create_unique_cmd(
        &c3::function_3, c3_obj, std::string{ "test string" }, a);
    ASSERT_FE((cmd_fcn_3->execute() == "test string 17"),
              "Check for correct return value.");

    get_sum gsum;
    auto cmd_int_sum =
        command_factory::create_unique_cmd(&get_sum::sum1, gsum, 10, 20);
    int sum_result = cmd_int_sum->execute();
    ASSERT_FE((cmd_int_sum->execute() == 30),
              "Check for correct return value.");

    get_const gconst;
    auto cmd_get_17 =
        command_factory::create_unique_cmd(&get_const::get_17, gconst);
    ASSERT_FE((sum_result + cmd_get_17->execute() == 47),
              "Check for correct return and operation value.");

    return 0;
};