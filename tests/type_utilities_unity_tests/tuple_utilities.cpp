/*!
*
* @author spades
* @date 16/03/23
*
*/
#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>
#include <sstream>

int main(){

    std::tuple<int, float, std::size_t> tpl(1, 2, 3);

    std::stringstream ss;
    utilities::print_tuple_elements(ss, tpl, " ");

    ASSERT_FE(ss.str() == "1 2 3 ", "");

    return 0;

};