/*!
*
* @author spades
* @date 13/03/23
*
*/

#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <memory.h>

template <utilities::is_execution_request T>
struct test {};

struct request1 {
    void operator()(){};
};

struct request2 {
    void execute(){};
};

struct request3 {
    void run(){};
};

int main()
{
    using type1 = test<request1>;
    using type2 = test<std::variant<request1, request2, request3>>;

    using rq1_ptr = std::unique_ptr<request1>;
    using rq2_ptr = std::unique_ptr<request2>;
    using rq3_ptr = std::unique_ptr<request3>;

    rq1_ptr test;

    //FUNCTOR===================================================================
    ASSERT_FE((utilities::is_smart_pointer<rq1_ptr>::value),
              "Verify if a given type is a smart pointer.");
    ASSERT_FE(
        (std::is_same_v<utilities::extract_type_from_smart_pointer<rq1_ptr>::type,
                        request1>),
        "Verify if smart pointer handled type is extracted correctly.");
    ASSERT_FE(utilities::is_functor<rq1_ptr>,
              "Verify if smart pointer points to a functor.");
    ASSERT_FE(utilities::is_execution_request<rq1_ptr>,
              "Verify is smart pointer points to an execution request.");

    //HAS EXECUTE METHOD========================================================
    ASSERT_FE((utilities::is_smart_pointer<rq2_ptr>::value),
              "Verify if a given type is a smart pointer.");
    ASSERT_FE(
        (std::is_same_v<utilities::extract_type_from_smart_pointer<rq2_ptr>::type,
                        request2>),
        "Verify if smart pointer handled type is extracted correctly.");
    ASSERT_FE(
        utilities::has_execute_method<rq2_ptr>,
        "Verify if smart pointer points to an object with execute member function.");
    ASSERT_FE(utilities::is_execution_request<rq2_ptr>,
              "Verify is smart pointer points to an execution request.");

    //HAS RUN METHOD============================================================
    ASSERT_FE((utilities::is_smart_pointer<rq3_ptr>::value),
              "Verify if a given type is a smart pointer.");
    ASSERT_FE(
        (std::is_same_v<utilities::extract_type_from_smart_pointer<rq3_ptr>::type,
                        request3>),
        "Verify if smart pointer handled type is extracted correctly.");
    ASSERT_FE(
        utilities::has_run_method<rq3_ptr>,
        "Verify if smart pointer points to an object with execute member function.");
    ASSERT_FE(utilities::is_execution_request<rq3_ptr>,
              "Verify is smart pointer points to an execution request.");

    return 0;
};