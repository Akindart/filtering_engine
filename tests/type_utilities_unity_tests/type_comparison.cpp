/*!
*
* @author spades
* @date 29/05/23
*
*/

#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

struct obj1 {};

struct obj2 : obj1 {};
struct obj3 : obj1 {};
struct obj4 : obj1 {};

int main()
{
    using info_type = std::variant<obj1, obj2, obj3, obj4>;
    ASSERT_FE(
        (utilities::is_convertible_v<info_type, obj1>),
        "Assert all type composing a variant is convertible into another.");

    return 0;
};
