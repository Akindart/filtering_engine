/*!
*
* @author spades
* @date 13/03/23
*
*/

#include "include/utilities/thread_pool.hpp"
#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/utilities/assertion_machinery.hpp"

struct obj1 {};
struct obj2 {};
struct obj3 {};

template <typename... Args>
struct test {
    int a;
};

int main()
{
    using info_type = test<obj1, obj2, obj3>;

    ASSERT_FE(
        (std::is_same_v<utilities::get_first_template_arg_t<info_type>, obj1>),
        "");

    return 0;
};