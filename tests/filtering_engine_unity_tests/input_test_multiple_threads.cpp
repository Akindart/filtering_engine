/*!
*
* @author spades
* @date 26/06/23
*
*/

#include "include/filtering_engine.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <chrono>
#include <atomic>

struct pose2D {
    std::atomic<float> x, y, theta;

    pose2D(pose2D &st){};
    pose2D(pose2D &&st){};
    pose2D(float x, float y, float theta)
        : x(x)
        , y(y)
        , theta(theta){};

    pose2D()
        : x(0)
        , y(0)
        , theta(0){};

    void reset()
    {
        x = 0;
        y = 0;
        theta = 0;
    };

    friend bool operator==(pose2D const &lhs, pose2D const &rhs)
    {
        return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.theta == rhs.theta);
    }

    friend std::ostream &operator<<(std::ostream &os, pose2D const &obj)
    {
        os << "State is \n";
        os << "x: " << obj.x << "\n";
        os << "y: " << obj.y << "\n";
        os << "theta: " << obj.theta << "\n";

        return os;
    }
};

struct info_type1 {
    float d_x, d_y, d_theta;

    friend std::ostream &operator<<(std::ostream &os, info_type1 const &obj)
    {
        os << "Hello, there! The displacement is \n";
        os << "d_x: " << obj.d_x << "\n";
        os << "d_y: " << obj.d_y << "\n";
        os << "d_theta: " << obj.d_theta << "\n";

        return os;
    }
};

struct update_state_signal {
    friend std::ostream &operator<<(std::ostream &os,
                                    update_state_signal const &obj)
    {
        os << "update_state_signal\n";
        return os;
    }
};

using pred_type_list = filtering_engine::prediction_input_list<info_type1>;
using cor_type_list =
    filtering_engine::correction_input_list<update_state_signal>;

using test_engine_t =
    filtering_engine::filtering_engine<pose2D, pred_type_list, cor_type_list,
                                       10, 10, 10, 10>;

template <>
template <>
void test_engine_t::state_update_handler_correction::update_state(
    std::shared_ptr<pose2D> &state, update_state_signal &&info_type)
{
    state->x.fetch_add(37, std::memory_order_relaxed);
}

template <>
template <>
void test_engine_t::state_update_handler_prediction::update_state(
    std::shared_ptr<pose2D> &state, info_type1 const &info_type)
{
    state->x.fetch_add(info_type.d_x, std::memory_order_relaxed);
    state->y.fetch_add(info_type.d_y, std::memory_order_relaxed);
    state->theta.fetch_add(info_type.d_theta, std::memory_order_relaxed);

    if (has_thread_pools() && (int)state->x % 7 == 0)
        forward_command<0>(state, update_state_signal{});
}

int main()
{
    using namespace std::chrono_literals;

    test_engine_t test_engine;

    const info_type1 info{ 1, 2, 3 };

    test_engine.start();

    for (std::size_t i = 1; i <= 100; ++i) {
        test_engine.input(info);
        std::this_thread::sleep_for(1ns);
    }

    std::this_thread::sleep_for(100ms);
    if (!(pose2D{ 803, 200, 300 } == *test_engine.get_state())){
        std::cout << "State is \n";
        std::cout << "x: " << test_engine.get_state()->x << "\n";
        std::cout << "y: " << test_engine.get_state()->y << "\n";
        std::cout << "theta: " << test_engine.get_state()->theta << "\n";
    }

    ASSERT_FE((pose2D{ 803, 200, 300 } == *test_engine.get_state()), "");

    test_engine.reset_state();

    std::this_thread::sleep_for(10ms);

    ASSERT_FE((pose2D{} == *test_engine.get_state()), "");

    test_engine.stop();

    return 0;
}