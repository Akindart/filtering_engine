/*!
*
* @author spades
* @date 05/03/23
*
*/

#include "include/utilities/thread_pool.hpp"
#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>

struct obj1{
    void run(){};
};
struct obj2{
    void execute(){};
};
struct obj3{
    void operator()(){};
    void execute();
};
struct obj4{};

int main(){

    using info_type = std::variant<obj1, obj2, obj3>;

    using qt = data_structures::cyclic_constant_size_queue<info_type, 4>;

    utilities::thread_pool<1, qt> pool;

    pool.insert_task(obj1{});
    pool.insert_task(obj2{});
    pool.insert_task(obj3{});

    ASSERT_FE(pool.get_task_queue_size() == 3, "");

    return 0;

};