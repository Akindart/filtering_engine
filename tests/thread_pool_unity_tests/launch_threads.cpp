/*!
*
* @author spades
* @date 15/03/23
*
*/

#include "include/utilities/thread_pool.hpp"
#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>
#include <chrono>
#include <sstream>

struct obj1 {
    static std::string str;
    void run() const
    {
        obj1::str.append("Obj1");
    };
};
std::string obj1::str;

struct obj2 {
    static std::string str;
    void execute() const
    {
        obj2::str.append("Obj2");
    };
};
std::string obj2::str;

struct obj3 {
    static std::string str;
    void operator()() const
    {
        obj3::str.append("Obj3");
    };
};
std::string obj3::str;

int main()
{
    using namespace std::chrono_literals;

    using info_type = std::variant<obj1, obj2, obj3>;

    using qt = data_structures::cyclic_constant_size_queue<info_type, 4>;

    std::stringstream ss;
    utilities::thread_pool<1, qt> pool;

    pool.launch();

    obj1 ob1;
    obj2 ob2;
    obj3 ob3;

    pool.insert_task(ob1);
    std::this_thread::sleep_for(100ms);
    pool.insert_task(ob2);
    std::this_thread::sleep_for(100ms);
    pool.insert_task(ob3);
    std::this_thread::sleep_for(100ms);
    pool.insert_task(obj3{});
    std::this_thread::sleep_for(100ms);

    ss << obj1::str << " " << obj2::str << " " << obj3::str;
    std::cout << ss.str() << std::endl;

    ASSERT_FE(ss.str() == "Obj1 Obj2 Obj3Obj3", "");
    ASSERT_FE(pool.get_task_queue_size() == 0, "");

    pool.stop_all_threads();
    pool.insert_task(ob3);
    pool.insert_task(ob3);

    ASSERT_FE(pool.get_task_queue_size() == 2, "");

    return 0;
};