list(APPEND ExecNameList${CMAKE_CURRENT_LIST_DIR}
     thread_pool_with_command
     filtering_engine_with_lockable_resource
     )

foreach (name IN LISTS ExecNameList${CMAKE_CURRENT_LIST_DIR})
    set(test_name test_${name})
    add_executable(${name} ${CMAKE_CURRENT_LIST_DIR}/${name}.cpp)
    add_dependencies(${name} filtering_engine)
    target_link_libraries(${name} filtering_engine)
    add_test(NAME ${test_name} COMMAND ${name})
    list(APPEND unity_test_list ${name})
    set_property(TEST ${test_name} PROPERTY LABELS INTEGRATION_TESTS)
endforeach ()
#
#list(APPEND ExecNameListFail${CMAKE_CURRENT_LIST_DIR}
#
#     )
#
#foreach(name IN LISTS ExecNameListFail${CMAKE_CURRENT_LIST_DIR})
#    set(test_name test_${name})
#    add_executable(${name} ${CMAKE_CURRENT_LIST_DIR}/${name}.cpp)
#    add_dependencies(${name} filtering_engine)
#    target_link_libraries(${name} filtering_engine)
#    add_test(NAME ${test_name} COMMAND ${name})
#    list(APPEND unity_test_list ${name})
#    set_property(TEST ${test_name} PROPERTY LABELS BUFFER_UNITY_TESTS)
#    set_property(TEST  ${test_name} PROPERTY WILL_FAIL TRUE)
#endforeach()

#add_executable(sensor_info_defined_wrong_temp_spec ${CMAKE_CURRENT_LIST_DIR}/sensor_info_defined_wrong_temp_spec.cpp)
#add_dependencies(sensor_info_defined_wrong_temp_spec filtering_engine)
#set_target_properties(sensor_info_defined_wrong_temp_spec
#                      PROPERTIES EXCLUDE_FROM_ALL TRUE EXCLUDE_FROM_DEFAULT_BUILD TRUE)
#target_link_libraries(sensor_info_defined_wrong_temp_spec filtering_engine)
#add_test(NAME test_sensor_info_defined_wrong_temp_spec
#         COMMAND ${CMAKE_COMMAND} --build . --target sensor_info_defined_wrong_temp_spec --config $<CONFIGURATION>
#         WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
#         )
#set_property(TEST test_sensor_info_defined_wrong_temp_spec
#             PROPERTY LABELS FRONTEND)
#set_property(TEST test_sensor_info_defined_wrong_temp_spec
#             PROPERTY WILL_FAIL TRUE)
