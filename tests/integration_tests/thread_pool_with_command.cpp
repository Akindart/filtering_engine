/*!
*
* @author spades
* @date 09/06/23
*
*/

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/utilities/assertion_machinery.hpp"
#include "include/utilities/thread_pool.hpp"
#include "include/utilities/command.hpp"

#include <chrono>

using namespace std::chrono_literals;

struct c1 {
    std::string *buffer;

    int function_1(int &)
    {
        //        std::this_thread::sleep_for(100ms);

        buffer->append("exe func 1\n");
        //        std::cout << "exe func 1" << std::endl;
        return 0;
    }
    template <typename T>
    int execute(T &&)
    {
        return 0;
    }
};

struct c2 {
    std::string *buffer;
    std::string function_2(std::string &&str)
    {
        //        std::this_thread::sleep_for(100ms);

        buffer->append("exe func 2 ").append(str).append("\n");
        //        std::cout << "exe func 2 " << str << std::endl;
        return { str };
    }
};

struct c3 {
    std::string *buffer;
    std::string function_3(std::string &&str, int &i)
    {
        std::stringstream ss;

        //        std::this_thread::sleep_for(100ms);

        //        std::cout << "exe func 3 " << str << " " << i  << std::endl;

        ss << "exe func 3 " << str << " " << i << "\n";

        buffer->append(ss.str());

        return ss.str();
    }
};

template <typename S>
struct c4 {
    std::string *buffer;

    template <typename T>
    std::string function_4(S &str, T &&i)
    {
        std::stringstream ss;

        ss << "exe func 4 " << str << " " << i << "\n";

        buffer->append(ss.str());

        return ss.str();
    }
};

int main()
{
    using namespace utilities;

    std::string global_buffer;

    c1 c1_obj{ &global_buffer };
    c2 c2_obj{ &global_buffer };
    c4<std::string> c4_obj{ &global_buffer };

    using qt = data_structures::cyclic_constant_size_queue<
        std::variant<
            command_factory::command_type<decltype(&c1::function_1)>::unq_ptr,
            command_factory::command_type<decltype(&c2::function_2)>::unq_ptr,
            decltype(command_factory::create_unique_cmd(
                &c3::function_3, c3{ &global_buffer }, std::string{}, (int)0)),
            command_factory::command_type<
                decltype(&c4<std::string>::function_4<int>)>::unq_ptr>,
        10>;

    using test_thread_pool_t = thread_pool<1, qt>;

    test_thread_pool_t test_thread_pool;

    int a = 10;

    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c1::function_1, c1_obj, a));

    test_thread_pool.launch();

    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c1::function_1, c1_obj, a));
    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c2::function_2, c2_obj, std::string{ "first" }));
    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c3::function_3, c3{ &global_buffer }, std::string{ "first" },
        (int)10));

    std::string str{ "first" };
    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c4<std::string>::function_4<int>, c4_obj, str, (int)10));

    std::this_thread::sleep_for(10ms);
    test_thread_pool.stop_all_threads();

    if (test_thread_pool.get_task_queue_size() != 0){
        std::cout << "Num of tasks:  " << test_thread_pool.get_task_queue_size() << std::endl;
    }

    ASSERT_FE(test_thread_pool.get_task_queue_size() == 0,
              "Thread pool should be empty by now.");

    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c1::function_1, c1_obj, a));
    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c2::function_2, c2_obj, std::string{ "second" }));
    test_thread_pool.insert_task(utilities::command_factory::create_unique_cmd(
        &c3::function_3, c3{ &global_buffer }, std::string{ "second" },
        (int)40));

    ASSERT_FE(test_thread_pool.get_task_queue_size() == 3,
              "Thread pool should have three commands in the execution line.");

    test_thread_pool.launch();
    std::this_thread::sleep_for(10ms);
    test_thread_pool.stop_all_threads();

    std::string test_buffer = R"(exe func 1
exe func 1
exe func 2 first
exe func 3 first 10
exe func 4 first 10
exe func 1
exe func 2 second
exe func 3 second 40
)";

    if (global_buffer != test_buffer) {
        std::cout << global_buffer << std::endl;
        std::cout << "================================" << std::endl;
        std::cout << test_buffer << std::endl;
    }
    ASSERT_FE(
        global_buffer == test_buffer,
        "Test buffer and global buffer altered using thread pool do not match");

    return 0;
}