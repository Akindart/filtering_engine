/*!
*
* @author spades
* @date 29/06/23
*
*/

#include "include/filtering_engine.hpp"
#include "include/utilities/lockable_resource.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <chrono>
#include <atomic>

struct pose2D {
    float x;
    float y;
    float theta;

    pose2D(pose2D const &p2D)
        : x(p2D.x)
        , y(p2D.y)
        , theta(p2D.theta){};

    pose2D(pose2D &&p2D)
        : x(p2D.x)
        , y(p2D.y)
        , theta(p2D.theta){};

    pose2D(float x, float y, float theta)
        : x(x)
        , y(y)
        , theta(theta){};

    pose2D()
        : x(0)
        , y(0)
        , theta(0){};

    void reset()
    {
        x = 0;
        y = 0;
        theta = 0;
    };

    friend bool operator==(pose2D const &lhs, pose2D const &rhs)
    {
        return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.theta == rhs.theta);
    }

    friend std::ostream &operator<<(std::ostream &os, pose2D const &obj)
    {
        os << "Hello, there! Pose2D is \n";
        os << "d_x: " << obj.x << "\n";
        os << "d_y: " << obj.y << "\n";
        os << "d_theta: " << obj.theta << "\n";

        return os;
    }
};

struct pose {
    utilities::lockable_resource<pose2D> pose_2d;

    pose(pose2D &st){};
    pose(pose2D &&st){};
    pose(float x, float y, float theta)
        : pose_2d(pose2D{ x, y, theta }){};

    pose()
        : pose(pose2D{ 0, 0, 0 }){};
    ;

    void reset()
    {
        pose_2d.lock_and_use([](pose2D &pose) { pose.reset(); });
    };

    friend bool operator==(pose const &lhs, pose const &rhs)
    {
        return lhs.pose_2d == rhs.pose_2d;
    }

    friend std::ostream &operator<<(std::ostream &os, pose const &obj)
    {
        obj.pose_2d.lock_and_use([&os](pose2D const &pose) {
            os << "State is \n";
            os << "x: " << pose.x << "\n";
            os << "y: " << pose.y << "\n";
            os << "theta: " << pose.theta << "\n";
        });

        return os;
    }
};

struct info_type1 {
    float d_x, d_y, d_theta;

    friend std::ostream &operator<<(std::ostream &os, info_type1 const &obj)
    {
        os << "Hello, there! The displacement is \n";
        os << "d_x: " << obj.d_x << "\n";
        os << "d_y: " << obj.d_y << "\n";
        os << "d_theta: " << obj.d_theta << "\n";

        return os;
    }
};

struct update_state_signal {
    friend std::ostream &operator<<(std::ostream &os,
                                    update_state_signal const &obj)
    {
        os << "update_state_signal\n";
        return os;
    }
};

using pred_type_list = filtering_engine::prediction_input_list<info_type1>;
using cor_type_list =
    filtering_engine::correction_input_list<update_state_signal>;

using test_engine_t =
    filtering_engine::filtering_engine<pose, pred_type_list, cor_type_list, 10,
                                       10, 10, 10>;

template <>
template <>
void test_engine_t::state_update_handler_correction::update_state(
    std::shared_ptr<pose> &state, update_state_signal &&update_state_signal)
{
    state->pose_2d.lock_and_use([](pose2D &pose) { pose.x += 37; });
}

template <>
template <>
void test_engine_t::state_update_handler_prediction::update_state(
    std::shared_ptr<pose> &state, info_type1 &&info_type)
{
    auto tmp_obj = *this;

    state->pose_2d.lock_and_use([&info_type, &state, &tmp_obj](pose2D &pose) {
        pose.x += info_type.d_x;
        pose.y += info_type.d_y;
        pose.theta += info_type.d_theta;

        if (tmp_obj.has_thread_pools() && (int)pose.x % 7 == 0)
            tmp_obj.forward_command<0>(state, update_state_signal{});
    });
}

int main()
{
    using namespace std::chrono_literals;

    test_engine_t test_engine;

    test_engine.start();

    for (std::size_t i = 1; i <= 100; ++i) {
        test_engine.input(info_type1{ 1, 2, 3 });
        std::this_thread::sleep_for(1ns);
    }

    std::this_thread::sleep_for(100ms);
    ASSERT_FE((pose{ 803, 200, 300 } == *test_engine.get_state()), "assert");

    test_engine.reset_state();

    std::this_thread::sleep_for(1ns);

    ASSERT_FE((pose{} == *test_engine.get_state()), "assert");

    test_engine.stop();

    return 0;
}