/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/utilities/lockable_resource.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>

template <typename T>
struct adder {
    void add_ten_plus_const(T const_val, T &info) const
    {
        info += 10 + const_val;
    }
};

int main()
{
    utilities::lockable_resource resource(0);
    adder<int> add;
    resource.use(&adder<int>::add_ten_plus_const, add, 10);
    ASSERT_FE(resource == 20, "");

    return 0;
}
