/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/utilities/lockable_resource.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>

template <typename T>
T add_ten_plus_const(T const_val, T &info)
{
    return info += 10 + const_val;
}

int main()
{
    int a = 0;
    utilities::lockable_resource<int> resource(a);
    resource.use(&add_ten_plus_const<int>, 10);
    ASSERT_FE(resource == 20, "");

    return 0;
}
