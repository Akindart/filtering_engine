/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/utilities/lockable_resource.hpp"
#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <regex>

template <typename T>
void print_resource_a(std::ostream &os, T info)
{
    using namespace std::chrono_literals;

    for (std::size_t i = 0; i < 20; ++i) {
        os << info << " "
           << "function"
           << "---" << std::endl;
        //        std::this_thread::sleep_for(100ms);
    }
}

template <typename T>
void print_resource_b(std::string &str, T info)
{
    using namespace std::chrono_literals;

    std::stringstream ss;
    ss << info;

    for (std::size_t i = 0; i < 20; ++i) {
        str.append(ss.str()).append("_function---\n");
        //        std::this_thread::sleep_for(100ms);
    }
}

template <typename T>
struct adder {
    void print_resource(std::ostream &os, T info)
    {
        using namespace std::chrono_literals;

        for (std::size_t i = 0; i < 20; ++i) {
            std::cout << info << " "
                      << "member_func"
                      << "---" << std::endl;
            //            std::this_thread::sleep_for(100ms);
        }
    }
    void print_resource_b(std::string &str, T info)
    {
        using namespace std::chrono_literals;

        std::stringstream ss;
        ss << info;

        for (std::size_t i = 0; i < 20; ++i) {
            str.append(ss.str()).append("_member_func---\n");
            //        std::this_thread::sleep_for(100ms);
        }
    }
};

int main()
{
    using namespace std::chrono_literals;
    using resource_type = utilities::lockable_resource<std::string &&>;

    resource_type resource("resource");

    int result = 0;

    adder<std::string &> add;

    std::regex reg_ex(
        "(resource_function---\\n)+(resource_member_func---\\n)+");

    std::string str2;

    std::thread first_lock([&resource, &str2](){
        resource.lock_and_use(&print_resource_b<std::string &>, str2);
    });
    std::thread second_lock([&resource, &str2](){
        resource.lock_and_use(&print_resource_b<std::string &>, str2);
    });
    std::this_thread::sleep_for(5ms);
    std::thread third_lock([&resource, &add, &str2](){
        resource.lock_and_use(&decltype(add)::print_resource_b, add, str2);
    });
    std::thread fourth_lock([&resource, &add, &str2](){
        resource.lock_and_use(&decltype(add)::print_resource_b, add, str2);
    });
    std::this_thread::sleep_for(5ms);

    first_lock.join();
    second_lock.join();
    third_lock.join();
    fourth_lock.join();

    if (!std::regex_match(str2, reg_ex)) {
        std::cout << str2 << std::endl;
    }

    ASSERT_FE((std::regex_match(str2, reg_ex)),"");

    return result;
}
