/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/utilities/lockable_resource.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>

template <typename T>
struct add_ten_plus_const {
    void operator()(T const_val, T &info) const
    {
        info += 10 + const_val;
    }
};

int main()
{
    utilities::lockable_resource resource(0);
    resource.use(add_ten_plus_const<int>(), 10);
    ASSERT_FE(resource == 20, "");

    return 0;
}
