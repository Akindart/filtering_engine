/*!
*
* @author spades
* @date 26/02/23
*
*/

#include "include/utilities/lockable_resource.hpp"
#include "include/utilities/assertion_machinery.hpp"

#include <iostream>

int main()
{
    utilities::lockable_resource resource(0);

    auto add_ten_plus_const = []<typename T>(T const_val, T &info) {
        info += 10 + const_val;
    };

    resource.use(add_ten_plus_const, 10);
    ASSERT_FE(resource == 20, "");

    return 0;
}