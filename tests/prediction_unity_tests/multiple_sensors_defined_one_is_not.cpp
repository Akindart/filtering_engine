/*!
*
* @author spades
* @date 16/02/23
*
*/

#include "include/prediction_handler.hpp"

#include <string>

namespace fe = filtering_engine;
using state_type = int;
using info_type_1 = int;
using info_type_2 = float;
using info_type_3 = std::string;
using info_type_4 = std::size_t;

using fph_int = fe::prediction_handler<state_type &&>;

template <>
template <>
void fph_int::update_state(int &&state, info_type_1 &info_type)
{
    std::cerr
        << "state_type&& state update for info_type_1 & info_type has run."
        << std::endl;
}
template <>
template <>
void fph_int::update_state(int &&state, info_type_2 const &info_type)
{
    std::cerr
        << "state_type&& state update for info_type_2 const & info_type has run."
        << std::endl;
}
template <>
template <>
void fph_int::update_state(int &&state, info_type_3 &&info_type)
{
    std::cerr
        << "state_type&& state update for info_type_3 && info_type has run."
        << std::endl;
}

int main(int argc, char **argv)
{
    int result = 0;

    auto filter_prediction_handler = fph_int{};

    try {
        info_type_1 data{};
        info_type_2 const data_const{};
        info_type_4 data_not_handled;
        filter_prediction_handler.update_state(0, data);
        filter_prediction_handler.update_state(0, data_const);
        filter_prediction_handler.update_state(0, info_type_3{});
        filter_prediction_handler.update_state(0, data_not_handled);

    } catch (std::invalid_argument const &e) {
        std::cerr << e.what() << std::endl;
        result = 1;
    }

    return result;
}