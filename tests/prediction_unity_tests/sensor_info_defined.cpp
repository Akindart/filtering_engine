/*!
*
* @author Pedro Daniel
* @date 16/02/23
*
*/

#include "include/prediction_handler.hpp"

namespace fe = filtering_engine;
using state_type = int;
using type_info_1 = int;

using fph_int = fe::prediction_handler<state_type &&>;

template <>
template <>
void fph_int::update_state(state_type &&state, type_info_1 &info_type)
{
    std::cerr << "state_type&& state update for test_type &  info_type has run."
              << std::endl;
}
template <>
template <>
void fph_int::update_state(state_type &&state, type_info_1 const &info_type)
{
    std::cerr
        << "state_type&& state update for test_type const & info_type has run."
        << std::endl;
}
template <>
template <>
void fph_int::update_state(state_type &&state, type_info_1 &&info_type)
{
    std::cerr << "state_type&& state update for test_type && info_type has run."
              << std::endl;
}

int main(int argc, char **argv)
{
    int result = 0;

    auto filter_prediction_handler = fph_int{};

    try {
        type_info_1 data{};
        type_info_1 const data_const{};
        filter_prediction_handler.update_state(0, data);
        filter_prediction_handler.update_state(0, data_const);
        filter_prediction_handler.update_state(0, 0);

    } catch (std::invalid_argument const &e) {
        std::cerr << e.what() << std::endl;
        result = 1;
    }

    return result;
}