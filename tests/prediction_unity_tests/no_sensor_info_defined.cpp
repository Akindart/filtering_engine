/*!
*
* @author Pedro Daniel
* @date 16/02/23
*
*/

#include "include/prediction_handler.hpp"

int main(int argc, char **argv)
{
    int result = 0;

    namespace fe = filtering_engine;

    auto filter_prediction_handler = fe::prediction_handler<int &&>();

    try {
        filter_prediction_handler.update_state(0, 0);

    } catch (std::invalid_argument const &e) {
        std::cerr << e.what() << std::endl;
        result = 1;
    }

    return result;
}