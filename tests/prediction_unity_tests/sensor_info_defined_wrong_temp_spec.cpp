/*!
*
* @author Pedro Daniel
* @date 16/02/23
*
*/

#include "include/prediction_handler.hpp"

namespace fe = filtering_engine;

using state_type = int;
using wrong_state_type = std::string;
using type_info_1 = int;

using fph_int = fe::prediction_handler<state_type>;

template<> template<>
void fph_int::update_state(state_type &&state, type_info_1 &&sensor_info){

    std::cerr << "Int&& state update for int sensor_info has run." << std::endl;

}

int main(int argc, char **argv){

    int result = 0;

    auto filter_prediction_handler = fph_int();

    try{
        filter_prediction_handler.update_state(wrong_state_type{}, type_info_1{});

    }
    catch (std::invalid_argument const &e){

        std::cerr << e.what() << std::endl;
        result = 1;

    }

    return result;

}