#ifndef PREDICTION_HANDLER_HPP
#define PREDICTION_HANDLER_HPP

#include <iostream>
#include <variant>
#include <stdexcept>

namespace filtering_engine
{

template <typename State>
class prediction_handler {
public:
    prediction_handler() = default;

    //    explicit frontend_handler(State &&){};

    template <typename InfoType>
    void update_state(State &&state, InfoType &&info_type)
    {
        throw std::invalid_argument{
            "Information type not handled in prediction step."
        };
    }

    template <typename InfoType, typename Data>
    void update_state(State &&state, InfoType &&info_type, Data &&data)
    {
        throw std::invalid_argument{
            "Information type, with extra data,  not handled in prediction step."
        };
    }
};

}

#endif //PREDICTION_HANDLER_HPP