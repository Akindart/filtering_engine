/*!
*
* @author spades
* @date 25/02/23
*
*/

#ifndef CYCLIC_CONSTANT_SIZE_QUEUE_HPP
#define CYCLIC_CONSTANT_SIZE_QUEUE_HPP

#include "include/type_utilities/type_utilities_base.hpp"

#include <array>
#include <stdexcept>
#include <ostream>
#include <variant>
#include <ranges>
#include <cassert>

namespace data_structures
{

template <typename T, std::size_t QueueSize,
          typename Container = std::array<T, QueueSize>>
class cyclic_constant_size_queue {
protected:
    using constant_queue = Container;
    using const_queue_it = typename constant_queue::iterator;

public:
    using value_type = T;

    cyclic_constant_size_queue()
        : _queue()
        , _front(_queue.end())
        , _next_pos(_queue.begin())
        , _back(_queue.end())
        , _queue_size(0){};

    explicit cyclic_constant_size_queue(Container &&c)
        : _queue(c)
        , _front(_queue.end())
        , _next_pos(_queue.begin())
        , _back(_queue.end())
        , _queue_size(0){};

    //    template <typename U>
    //        requires std::convertible_to<std::unique_ptr<U>, T> &&
    //                 utilities::is_variant_v<T>
    //    void push(std::unique_ptr<U> elem)
    //    {
    //        *_next_pos = elem;
    //        _update_it_on_insertion();
    //    }

    template <std::convertible_to<T> U>
    //        requires(!utilities::smart_pointer<U>)
    void push(U &&elem)
    {
        //        *_next_pos = std::forward<U>(elem);
        *_next_pos = std::forward<U>(elem);
        _update_it_on_insertion();
    }

    void drop_front()
    {
        if (empty())
            throw std::invalid_argument{ "Queue is empty!" };

        _update_it_on_pop();
    }

    T pop()
    {
        if (empty())
            throw std::invalid_argument{ "Queue is empty!" };

        T &result = *_front;

        _update_it_on_pop();

        return result;
    }

    bool empty()
    {
        return _front == _back && _queue_size == 0;
    }

    T const &front() const
    {
        return *_front;
    }

    T const &back() const
    {
        return *_back;
    }

    const_queue_it begin() const
    {
        return _front;
    }

    const_queue_it end() const
    {
        return _back;
    }

    std::size_t size()
    {
        return _queue_size;
    }

    constexpr std::size_t capacity()
    {
        return QueueSize;
    }

    friend inline std::ostream &operator<<(
        std::ostream &os,
        cyclic_constant_size_queue<T, QueueSize, Container> &lim_size_queue)
    {
        using this_queue_type =
            cyclic_constant_size_queue<T, QueueSize, Container>;

        auto inc_cyclic_queue_it =
            [&lim_size_queue](typename this_queue_type::const_queue_it &it) {
                if (++it == lim_size_queue._queue.end())
                    it = lim_size_queue._queue.begin();
            };

        if (!lim_size_queue.empty()) {
            auto it = lim_size_queue._front;
            do {
                os << *it << " ";
                inc_cyclic_queue_it(it);
            } while (it != lim_size_queue._next_pos);
        }

        return os;
    }

private:
    void _update_it_on_insertion()
    {
        if (_front == _back && _queue_size == 0) {
            _front = _next_pos;
            _back = _next_pos;
            ++_queue_size;
        } else if (_next(_next_pos) == _front) {
            _increment_it(_back);
            ++_queue_size;
        } else if (_next_pos == _front) {
            _increment_it(_front);
            _increment_it(_back);
        } else {
            _increment_it(_back);
            ++_queue_size;
        }
        _increment_it(_next_pos);

        assert((_queue_size <= QueueSize) &&
               "Cyclic constant queue size is out of bounds");
    }

    void _update_it_on_pop()
    {
        if (_front == _back) {
            _front = _queue.end();
            _back = _queue.end();
            --_queue_size;
        } else if (_next_pos == _front) {
            _increment_it(_front);
            --_queue_size;
        } else {
            _increment_it(_front);
            --_queue_size;
        }
        assert((_queue_size <= QueueSize) &&
               "Cyclic constant queue size is out of bounds");
    }

    const_queue_it _next(const_queue_it const &it)
    {
        const_queue_it result = it + 1;

        if (result == _queue.end())
            result = _queue.begin();

        return result;
    }

    void _increment_it(const_queue_it &it)
    {
        if (it == _queue.end() || ++it == _queue.end())
            it = _queue.begin();
    }

    constant_queue _queue;
    const_queue_it _front;
    const_queue_it _back;
    const_queue_it _next_pos;

    std::size_t _queue_size;
};

}
#endif //CYCLIC_CONSTANT_SIZE_QUEUE_HPP
