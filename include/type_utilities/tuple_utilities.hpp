/*!
*
* @author spades
* @date 16/03/23
*
*/

#ifndef TUPLE_UTILITIES_HPP
#define TUPLE_UTILITIES_HPP

#include <ostream>
#include <tuple>
#include <string>

namespace utilities
{

template <std::size_t Idx, std::size_t... Indices>
struct get_first_idx {
    static constexpr std::size_t value = Idx;
};

template <std::size_t Idx, std::size_t... Indices>
struct index_sequence_tail {
    using tail = std::index_sequence<Indices...>;
};

template <std::size_t... I, typename... Args>
inline std::ostream &
print_tuple_element(std::ostream &os, std::tuple<Args...> &&tuple_obj,
                    std::index_sequence<I...>,
                    std::string const &separator = { "\n" })
{
    if constexpr (sizeof...(I) >= 1) {
        constexpr std::size_t idx = get_first_idx<I...>::value;

        os << std::get<idx>(tuple_obj) << separator;
    }
    if constexpr (sizeof...(I) > 1) {
        using idx_tail = typename index_sequence_tail<I...>::tail;

        print_tuple_element(os, std::forward<std::tuple<Args...>>(tuple_obj),
                            idx_tail{}, separator);
    }

    return os;
}

template <std::size_t... I, typename... Args>
inline std::ostream &
print_tuple_element(std::ostream &os, std::tuple<Args...> &tuple_obj,
                    std::index_sequence<I...>,
                    std::string const &separator = { "\n" })
{
    if constexpr (sizeof...(I) >= 1) {
        constexpr std::size_t idx = get_first_idx<I...>::value;

        os << std::get<idx>(tuple_obj) << separator;
    }
    if constexpr (sizeof...(I) > 1) {
        using idx_tail = typename index_sequence_tail<I...>::tail;

        print_tuple_element(os, std::forward<std::tuple<Args...>>(tuple_obj),
                            idx_tail{}, separator);
    }

    return os;
}

template <typename... Args>
inline std::ostream &
print_tuple_elements_impl(std::ostream &os, std::tuple<Args...> &&tuple_obj,
                          std::string const &separator = { "\n" })
{
    print_tuple_element(os, std::forward<std::tuple<Args...>>(tuple_obj),
                        std::make_index_sequence<sizeof...(Args)>(), separator);

    return os;
}

template <typename... Args>
inline std::ostream &
print_tuple_elements_impl(std::ostream &os, std::tuple<Args...> &tuple_obj,
                          std::string const &separator = { "\n" })
{
    print_tuple_element(os, std::forward<std::tuple<Args...>>(tuple_obj),
                        std::make_index_sequence<sizeof...(Args)>(), separator);

    return os;
}

template <typename T>
inline std::ostream &
print_tuple_elements(std::ostream &os, T &&tuple_obj,
                     std::string const &separator = { "\n" })
{
    return print_tuple_elements_impl(os, std::forward<T>(tuple_obj), separator);
}

}
#endif //TUPLE_UTILITIES_HPP
