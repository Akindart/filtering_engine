/*!
*
* @author spades
* @date 01/05/23
*
*/

#ifndef CALLABLE_TRAITS_HPP
#define CALLABLE_TRAITS_HPP

#include <functional>

namespace utilities
{

template <typename T>
struct function_type : std::false_type {};

template <typename R, typename... Args>
struct function_type<R(Args...)> : std::true_type {
    using type = std::function<R(Args...)>;
};

template <typename R, typename... Args>
struct function_type<R (*)(Args...)> : std::true_type {
    using type = std::function<R(Args...)>;
};

template <typename R, typename C, typename... Args>
struct function_type<R (C::*)(Args...)> : std::true_type {
    using type = std::function<R(const C &, Args...)>;
};

template <typename R, typename C, typename... Args>
struct function_type<R (C::*&)(Args...)> : std::true_type {
    using type = std::function<R(const C &, Args...)>;
};

template <typename R, typename... Args>
struct function_type<R (&)(Args...)> : std::true_type {
    using type = std::function<R(Args...)>;
};

template <typename T>
struct function_type<std::function<T>> : std::true_type {
    using type = std::function<T>;
};

template <typename T>
using function_type_t = typename function_type<T>::type;

//==============================================================================
template <std::size_t Idx, typename... Args>
using nth_type_from_list_t =
    typename std::tuple_element_t<Idx, std::tuple<Args...>>;

//==============================================================================
template <size_t Idx, typename T>
struct function_type_element : std::false_type {};

template <size_t Idx, typename R, typename... Args>
    requires(Idx < sizeof...(Args))
struct function_type_element<Idx, R(Args...)> : std::true_type {
    using type = nth_type_from_list_t<Idx, Args...>;
};

template <size_t Idx, typename R, typename... Args>
    requires(Idx < sizeof...(Args))
struct function_type_element<Idx, std::function<R(Args...)>> : std::true_type {
    using type = nth_type_from_list_t<Idx, Args...>;
};

template <size_t Idx, typename T>
using function_type_element_t = typename function_type_element<Idx, T>::type;

//==============================================================================
template <typename T>
struct remove_reference_from_func_args : std::false_type {};

template <typename R, typename... Args>
struct remove_reference_from_func_args<R(Args...)> : std::true_type {
    using type = R(std::remove_reference_t<Args>...);
};

template <typename R, typename... Args>
struct remove_reference_from_func_args<R (*)(Args...)> : std::true_type {
    using type = R (*)(std::remove_reference_t<Args>...);
};

template <typename R, typename... Args>
struct remove_reference_from_func_args<R (&)(Args...)> : std::true_type {
    using type = R (&)(std::remove_reference_t<Args>...);
};

template <typename R, typename C, typename... Args>
struct remove_reference_from_func_args<R (C::*)(Args...)> : std::true_type {
    using type = R (C::*)(std::remove_reference_t<Args>...);
};

template <typename R, typename C, typename... Args>
struct remove_reference_from_func_args<std::function<R(const C &, Args...)>>
    : std::true_type {
    using type =
        std::function<R(const C &, typename std::remove_reference_t<Args>...)>;
};

template <typename T>
struct remove_reference_from_func_args<std::function<T>> : std::true_type {
    using type =
        std::function<typename remove_reference_from_func_args<T>::type>;
};

template <typename T>
using remove_reference_from_func_args_t =
    typename remove_reference_from_func_args<T>::type;

//==============================================================================
template <typename T>
struct remove_cvref_from_func_args : std::false_type {};

template <typename R, typename... Args>
struct remove_cvref_from_func_args<R(Args...)> : std::true_type {
    using type = R(std::remove_cvref_t<Args>...);
};

template <typename R, typename... Args>
struct remove_cvref_from_func_args<R (*)(Args...)> : std::true_type {
    using type = R (*)(std::remove_cvref_t<Args>...);
};

template <typename R, typename... Args>
struct remove_cvref_from_func_args<R (&)(Args...)> : std::true_type {
    using type = R (&)(std::remove_cvref_t<Args>...);
};

template <typename R, typename C, typename... Args>
struct remove_cvref_from_func_args<R (C::*)(Args...)> : std::true_type {
    using type = R (C::*)(std::remove_cvref_t<Args>...);
};

template <typename R, typename C, typename... Args>
struct remove_cvref_from_func_args<std::function<R(const C &, Args...)>>
    : std::true_type {
    using type =
        std::function<R(const C &, typename std::remove_cvref_t<Args>...)>;
};

template <typename T>
struct remove_cvref_from_func_args<std::function<T>> : std::true_type {
    using type = std::function<typename remove_cvref_from_func_args<T>::type>;
};

template <typename T>
using remove_cvref_from_func_args_t =
    typename remove_cvref_from_func_args<T>::type;

//==============================================================================
template <typename T>
struct callable_metadata_functor : std::false_type {
    using owner_type = std::false_type;
    static inline bool const is_member_callable = false;
};

template <typename Functor, typename Ret, typename... Params>
struct callable_metadata_functor<Ret (Functor::*)(Params...)> : std::true_type {
    using type = Ret(Params...);
    using return_type = Ret;
    using param_set = std::tuple<Params...>;
    static inline std::size_t const num_params = sizeof...(Params);
};

template <typename Functor, typename Ret, typename... Params>
struct callable_metadata_functor<Ret (Functor::*)(Params...) const>
    : std::true_type {
    using type = Ret(Params...);
    using return_type = Ret;
    using param_set = std::tuple<Params...>;
    static inline std::size_t const num_params = sizeof...(Params);
};

template <typename Functor, typename Ret, typename... Params>
struct callable_metadata_functor<Ret (Functor::*)(Params...) const &>
    : std::true_type {
    using type = Ret(Params...);
    using return_type = Ret;
    using param_set = std::tuple<Params...>;
    static inline std::size_t const num_params = sizeof...(Params);
};

template <typename Functor, typename Ret, typename... Params>
struct callable_metadata_functor<Ret (Functor::*)(Params...) &>
    : std::true_type {
    using type = Ret(Params...);
    using return_type = Ret;
    using param_set = std::tuple<Params...>;
    static inline std::size_t const num_params = sizeof...(Params);
};

template <typename T>
struct callable_metadata_aux
    : callable_metadata_functor<decltype(&T::operator())> {};

template <typename R, typename... Args>
struct callable_metadata_aux<R(Args...)> : std::true_type {
    using type = R(Args...);
    using return_type = R;
    using param_set = std::tuple<Args...>;
    static inline std::size_t const num_params = sizeof...(Args);
};

template <typename R, typename... Args>
struct callable_metadata_aux<R (*)(Args...)> : std::true_type {
    using type = R(Args...);
    using return_type = R;
    using param_set = std::tuple<Args...>;
    static inline std::size_t const num_params = sizeof...(Args);
};

template <typename R, typename C, typename... Args>
struct callable_metadata_aux<R (C::*)(Args...) const> : std::true_type {
    using type = R (C::*)(Args...);
    using return_type = R;
    using owner_type = C;
    using param_set = std::tuple<Args...>;
    static inline std::size_t const num_params = sizeof...(Args);
    static inline bool const is_member_callable = true;
};

template <typename R, typename C, typename... Args>
struct callable_metadata_aux<R (C::*)(Args...)> : std::true_type {
    using type = R (C::*)(Args...);
    using return_type = R;
    using owner_type = C;
    using param_set = std::tuple<Args...>;
    static inline std::size_t const num_params = sizeof...(Args);
    static inline bool const is_member_callable = true;
};

template <typename R, typename C, typename... Args>
struct callable_metadata_aux<R (C::*&)(Args...)> : std::true_type {
    using type = R (C::*)(Args...);
    using return_type = R;
    using owner_type = C;
    using param_set = std::tuple<Args...>;
    static inline std::size_t const num_params = sizeof...(Args);
    static inline bool const is_member_callable = true;
};

template <typename R, typename... Args>
struct callable_metadata_aux<R (&)(Args...)> : std::true_type {
    using type = R (&)(Args...);
    using return_type = R;
    using param_set = std::tuple<Args...>;
    static inline std::size_t const num_params = sizeof...(Args);
};

template <typename T>
struct callable_metadata : callable_metadata_aux<T> {};

template <typename T>
struct callable_metadata<std::function<T>> : callable_metadata_aux<T> {
    using type = std::function<T>;
};

template <typename T>
using function_signature_t = typename callable_metadata<T>::type;

}
#endif //CALLABLE_TRAITS_HPP
