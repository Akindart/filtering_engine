/*!
*
* @author spades
* @date 13/03/23
*
*/

#ifndef EXECUTABLE_TRAITS_H
#define EXECUTABLE_TRAITS_H

#include "type_utilities_base.hpp"

namespace utilities
{

template <typename T>
concept has_execute_method = requires(T v) {
    std::is_invocable_v<
        decltype(&utilities::extract_type_from_smart_pointer_t<T>::execute),
        typename utilities::extract_type_from_smart_pointer_t<T>>;
};

template <typename T>
concept has_run_method = requires(T v) {
    std::is_invocable_v<
        decltype(&utilities::extract_type_from_smart_pointer_t<T>::run),
        typename utilities::extract_type_from_smart_pointer_t<T>>;
};

template <typename T>
concept is_functor = requires(T v) {
    std::is_invocable_v<
        decltype(&utilities::extract_type_from_smart_pointer_t<T>::operator()),
        typename utilities::extract_type_from_smart_pointer_t<T>>;
};

template <typename T>
concept is_execution_request_simple =
    has_execute_method<T> || has_run_method<T> || is_functor<T>;

//==============================================================================
template <typename T>
struct is_variant_executable : std::false_type {};

template <typename... Args>
    requires(is_execution_request_simple<Args> && ...)
struct is_variant_executable<std::variant<Args...>> : std::true_type {};

template <typename T>
inline constexpr bool is_variant_executable_v = is_variant_executable<T>::value;

template <typename T>
concept is_execution_request_variant = is_variant_executable_v<T>;

//==============================================================================
template <typename... Args>
concept is_execution_request = ((is_execution_request_simple<Args> ||
                                 is_execution_request_variant<Args>)&&...);

}

#endif //EXECUTABLE_TRAITS_H
