/*!
*
* @author spades
* @date 08/03/23
*
*/

#ifndef FIFO_LIKE_CONTAINER_TRAITS_HPP
#define FIFO_LIKE_CONTAINER_TRAITS_HPP

#include "type_utilities_base.hpp"

namespace utilities
{

template <typename T, typename ValueType = typename T::value_type>
concept has_push_method =
    requires(T v, ValueType p) { v.push(std::forward<ValueType>(p)); };

template <typename T, typename ValueType = typename T::value_type>
concept has_push_back_method =
    requires(T v, ValueType p) { v.push_back(std::forward<ValueType>(p)); };

template <typename T, typename ValueType = typename T::value_type>
concept has_emplace_method =
    requires(T v, ValueType p) { v.emplace(std::forward<ValueType>(p)); };

template <typename T>
concept has_back_insert_method =
    (has_push_method<T> || has_push_back_method<T> || has_emplace_method<T>);
//=============================================================================
template <typename T, typename ValueType = typename T::value_type>
concept has_pop_method = requires(T v) {
    std::convertible_to<std::invoke_result_t<decltype(&T::pop), T>, ValueType>;
};

template <typename T, typename ValueType = typename T::value_type>
concept has_pop_front_method = requires(T v) {
    std::convertible_to<std::invoke_result_t<decltype(&T::pop_front), T>,
                        ValueType>;
};

template <typename T, typename ValueType = typename T::value_type>
concept has_front_method = requires(T v) {
    std::convertible_to<std::invoke_result_t<decltype(&T::front), T>,
                        ValueType const &>;
};

template <typename T>
concept fifo_like_remove =
    (has_pop_method<T> || has_pop_front_method<T>)&&has_front_method<T>;
//=============================================================================

template <typename T>
concept has_empty_method = requires(T v) {
    std::convertible_to<std::invoke_result_t<decltype(&T::empty), T>, bool>;
};

template <typename T>
concept has_size_method = requires(T v) {
    {
        v.size()
    } -> std::convertible_to<std::size_t>;
};

template <typename T>
concept has_capacity_method = requires(T v) {
    {
        v.capacity()
    } -> std::convertible_to<std::size_t>;
};

template <typename T>
concept has_max_size_method = requires(T v) {
    {
        v.max_size()
    } -> std::convertible_to<std::size_t>;
};

template <typename T>
concept can_watch_size = has_empty_method<T> && has_size_method<T> &&
                         (has_capacity_method<T> || has_max_size_method<T>);
//=============================================================================
template <typename T>
concept fifo_like_container =
    has_back_insert_method<T> && fifo_like_remove<T> && can_watch_size<T>;

}

#endif //FIFO_LIKE_CONTAINER_TRAITS_HPP
