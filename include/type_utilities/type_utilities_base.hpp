/*!
*
* @author spades
* @date 26/02/23
*
*/

#ifndef TYPE_UTILITIES_BASE_HPP
#define TYPE_UTILITIES_BASE_HPP

#include <variant>
#include <memory>

namespace utilities
{
template <typename Head, typename... Args>
struct head {
    using type = Head;
};

template <typename T>
struct get_first_template_arg : std::false_type {};

template <template <typename...> typename T, typename U, typename... Args>
struct get_first_template_arg<T<U, Args...>> : std::true_type {
    using type = U;
};

template <typename T>
using get_first_template_arg_t = typename get_first_template_arg<T>::type;

template <typename T>
inline constexpr bool get_first_template_arg_v =
    get_first_template_arg<T>::value;
//=============================================================================
struct get_callable_type {
    template <typename C>
    constexpr static C type(C &&)
    {
        return C{};
    };
};
//=============================================================================
template <typename T>
struct is_variant : std::false_type {};

template <typename... Args>
struct is_variant<std::variant<Args...>> : std::true_type {};

template <typename T>
inline constexpr bool is_variant_v = is_variant<T>::value;

template <typename T>
concept variant_type = is_variant_v<T>;
//=============================================================================
template <typename T, typename... Args>
struct is_type_in_pack {
    static constexpr bool value{ (std::is_same_v<T, Args> || ...) };
};

template <typename T, typename... Args>
inline constexpr bool is_type_in_pack_v = is_type_in_pack<T, Args...>::value;

template <typename T, typename... Args>
concept type_in_pack = is_type_in_pack_v<T, Args...>;
//=============================================================================
template <typename T, typename... Args>
struct is_type_in_variant : std::false_type {};

template <typename T, typename... Args>
struct is_type_in_variant<T, std::variant<Args...>> {
    static constexpr bool value = is_type_in_pack_v<T, Args...>;
};

template <typename T, typename V>
inline constexpr bool is_type_in_variant_v = is_type_in_variant<T, V>::value;

template <typename T, typename V>
concept type_in_variant = is_type_in_variant_v<T, V>;
//=============================================================================
template <typename F, typename T>
struct is_variant_convertible {
    static constexpr bool value{ std::is_convertible_v<F, T> };
};

template <typename T, typename... Args>
struct is_variant_convertible<std::variant<Args...>, T> {
    static constexpr bool value{ (std::is_convertible_v<Args, T> || ...) };
};

template <typename F, typename... Args>
struct is_variant_convertible<F, std::variant<Args...>> {
    static constexpr bool value{ (std::is_convertible_v<F, Args> || ...) };
};

template <typename F, typename T>
inline constexpr bool is_convertible_v = is_variant_convertible<F, T>::value;

template <typename F, typename T>
concept is_convertible = is_convertible_v<F, T>;
//=============================================================================
template <typename T>
struct is_smart_pointer_aux : std::false_type {};

template <typename T>
struct is_smart_pointer_aux<std::unique_ptr<T>> : std::true_type {};

template <typename T>
struct is_smart_pointer_aux<std::shared_ptr<T>> : std::true_type {};

template <typename T>
struct is_smart_pointer
    : is_smart_pointer_aux<std::remove_const_t<std::remove_cvref_t<T>>> {};

template <typename T>
concept smart_pointer = is_smart_pointer<T>::value;

template <bool B, typename T>
struct extract_type_from_smart_pointer_impl : std::false_type {
    using type = T;
};

template <typename T>
struct extract_type_from_smart_pointer_impl<true, std::unique_ptr<T>>
    : std::true_type {
    using type = T;
};

template <typename T>
struct extract_type_from_smart_pointer_impl<true, std::shared_ptr<T>>
    : std::true_type {
    using type = T;
};

template <typename T>
struct extract_type_from_smart_pointer {
    using type = typename extract_type_from_smart_pointer_impl<
        is_smart_pointer<T>::value,
        std::remove_const_t<std::remove_cvref_t<T>>>::type;
    static constexpr bool value =
        extract_type_from_smart_pointer_impl<smart_pointer<T>, T>::value;
};

template <typename T>
using extract_type_from_smart_pointer_t =
    typename extract_type_from_smart_pointer<T>::type;

}
#endif //TYPE_UTILITIES_BASE_HPP
