/*!
*
* @author user
* @date 27/04/23
*
*/

#ifndef ASSERTION_MACHINERY_HPP
#define ASSERTION_MACHINERY_HPP

#include <string>
#include <source_location>
#include <iostream>
#include <sstream>

namespace utilities
{

void assertion(const bool assert_result, std::string &&assert_condition,
               std::string &&assert_msg,
               std::source_location sl = std::source_location::current())
{
    if (assert_result)
        return;

    std::stringstream ss;

    ss << "Assertion Failed\n";
    ss << "Condition: " << assert_condition << "\n";
    if (!assert_msg.empty())
        ss << "Msg: " << assert_msg << "\n";
    ss << "In line " << sl.line() << ", file " << sl.file_name() << "\n";

    std::cout << ss.str() << std::endl;

#if defined(__clang__) || defined(__GNUC__) || defined(__INTEL_COMPILER)
    __builtin_trap();
#elif defined(_MSC_VER)
    __debugbreak();
#else
    std::abort();
#endif
};

}

#define ASSERT_FE(condition, assert_msg) \
    utilities::assertion(condition, std::string{ #condition }, assert_msg)

#endif //ASSERTION_MACHINERY_HPP
