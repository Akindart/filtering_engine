/*!
*
* @author spades
* @date 26/02/23
*
*/

#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "include/type_utilities/type_utilities_base.hpp"
#include "include/type_utilities/type_utilities.hpp"

#include <tuple>
#include <functional>
#include <iostream>
#include <memory>
#include <type_traits>

namespace utilities
{

class command_factory;

/***
 * @class command
 * @brief The idea is that command is created once and never copied. If one must
 * pass it into functions, use command_ptr to handle unique owner. A reference
 * wrapper is also provided, but not recommended.
 * @tparam C Callable entity.
 * @tparam CS Callable Signature entity. This is needed to correctly handle
 * functors and lambdas.
 * @tparam Args Arguments to be used when invoking C.
 */
template <typename C, typename CS, typename... Args>
    requires std::is_invocable_v<utilities::remove_cvref_from_func_args_t<CS>,
                                 std::remove_cvref_t<Args>...>
class command {
    using function_t = utilities::function_type_t<CS>;

    struct cmd_badge;

    template <typename T>
    struct member_func_pointer : std::false_type {};

    template <typename R, typename C_, typename... Args_>
    struct member_func_pointer<R (C_::*)(Args_...)> : std::true_type {};

    template <typename T>
    inline static constexpr bool _is_member_func_pointer_v =
        member_func_pointer<T>::value;

public:
    using ref = std::reference_wrapper<command<C, CS, Args...>>;
    using unq_ptr = std::unique_ptr<command<C, CS, Args...>>;

    command() = delete;
    command(command<C, CS, Args...> &) = delete;
    command(command<C, CS, Args...> &&) = delete;

    explicit command(command<C, CS, Args...>::cmd_badge &&, C callable,
                     Args &&...args)
        : command(callable, std::forward<Args>(args)...){};

    explicit command(command<C, CS, Args...>::cmd_badge &&)
        : _callable()
        , _args(std::tuple<std::remove_reference_t<Args>...>(
              std::remove_reference_t<Args>{}...)){};

    void operator=(command &) = delete;
    void operator=(command &&) = delete;

    /***
     * @brief This will execute the passed callable parameter and return the
     * result. Consider using a returning type that avoid copy for big objects.
     * @return
     */
    auto execute()
    {
        if constexpr (_is_member_func_pointer_v<C>)
            return _command_invoke(
                std::make_index_sequence<sizeof...(Args) - 1>());
        else
            return _command_invoke(std::make_index_sequence<sizeof...(Args)>());
    };

    friend inline std::ostream &operator<<(std::ostream &os, command &cmd)
    {
        os << "[ Command [ Parameters [ ";

        utilities::print_tuple_elements(os, cmd._args, " ");

        os << "] ] ]";

        return os;
    }

private:
    explicit command(C callable, Args &&...args)
        : _callable(callable)
        , _args(std::tuple<std::remove_reference_t<Args>...>(args...)){};

    /***
     * @brief Badge that allows the construction of command class.
     */
    struct cmd_badge {
        explicit cmd_badge(){};
    };

    friend class command_factory;

    template <typename T, typename U>
        requires std::is_rvalue_reference_v<std::remove_const_t<T>> &&
                 std::is_same_v<std::remove_cvref_t<U>, std::remove_cvref_t<T>>
    constexpr inline T &&_command_move(U &data)
    {
        return std::move(data);
    }

    template <typename T, typename U>
        requires(!std::is_rvalue_reference_v<std::remove_const_t<T>>) &&
                std::is_same_v<std::remove_cvref_t<U>, std::remove_cvref_t<T>>
    constexpr inline T &_command_move(U &data)
    {
        return data;
    }

    template <std::size_t... I>
    constexpr inline auto _command_invoke(std::index_sequence<I...>)
    {
        if constexpr (_is_member_func_pointer_v<C>)
            return std::invoke(
                _callable, std::get<0>(_args),
                _command_move<
                    utilities::function_type_element_t<I + 1, function_t>>(
                    std::get<I + 1>(_args))...);
        else
            return std::invoke(
                _callable,
                _command_move<utilities::function_type_element_t<I, function_t>>(
                    std::get<I>(_args))...);
    }

    C _callable;
    std::tuple<std::remove_reference_t<Args>...> _args;
};

template <typename T>
struct type_filter {
    using type = T;
};
template <typename T>
struct type_filter<T *&&> {
    using type = T *;
};

/***
 * @brief command_factory abstracts the creation of a command object. The
 * command class can only be instantiated using this command_factory.
 */
class command_factory {
protected:
public:
    template <typename C, typename CS = utilities::function_signature_t<C>,
              std::size_t... Idx>
    static constexpr auto inline generate_command_resultant_type(
        std::index_sequence<Idx...>)
    {
        using callable_meta_info_type = utilities::callable_metadata<C>;

        if constexpr (callable_meta_info_type::is_member_callable) {
            using command_type =
                command<C, CS, typename callable_meta_info_type::owner_type &,
                        std::tuple_element_t<
                            Idx, typename callable_meta_info_type::param_set>...>;
            using badge = typename command_type::cmd_badge;
            return command_type{ badge{} };
        } else {
            using command_type =
                command<C, CS,
                        std::tuple_element_t<
                            Idx, typename callable_meta_info_type::param_set>...>;
            using badge = typename command_type::cmd_badge;
            return command_type{ badge{} };
        }
    }

    ///This can only be used if all params of callable are trivially constructable
    template <typename C>
    using command_type = decltype(generate_command_resultant_type<C>(
        std::make_index_sequence<utilities::callable_metadata<C>::num_params>{}));

    template <typename C, typename CS = utilities::function_signature_t<C>,
              typename... Args>
    static inline constexpr
        typename command<C, CS, typename type_filter<Args &&>::type...>::unq_ptr
        create_unique_cmd(C &&c, Args &&...args)
    {
        using cmd_type = command<C, CS, typename type_filter<Args &&>::type...>;
        using badge = typename cmd_type::cmd_badge;
        return std::make_unique<cmd_type>(badge{}, c,
                                          std::forward<Args>(args)...);
    }
};

}

#endif //COMMAND_HPP
