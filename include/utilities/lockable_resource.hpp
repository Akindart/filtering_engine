/*!
*
* @author spades
* @date 26/02/23
*
*/

#ifndef LOCKABLE_RESOURCE_HPP
#define LOCKABLE_RESOURCE_HPP

#include <thread>
#include <mutex>
#include <shared_mutex>
#include <functional>
#include <type_traits>
#include <memory>

namespace utilities
{

/***
 * @brief
 * @tparam R Resource type, must have move and empty constructors.
 * @tparam M Mutex type to be used, must have move and empty constructors..
 */
template <typename R, typename M = std::mutex, typename L = std::unique_lock<M>>
class lockable_resource {
    using T = std::remove_reference_t<R>;
    using MutexType = std::remove_reference_t<M>;

public:
    //    template <typename... Args>
    //    lockable_resource(Args &&... args)
    //        : _resource(std::make_shared<Args &&...>(args...))
    //        , _resource_mutex(std::make_shared<MutexType>()){};
    lockable_resource()
        : _resource(std::make_shared<T>())
        , _resource_mutex(std::make_shared<MutexType>()){};
    explicit lockable_resource(R &resource)
        : _resource(std::make_shared<T>(std::forward<R>(resource)))
        , _resource_mutex(std::make_shared<MutexType>()){};
    explicit lockable_resource(R &&resource)
        : _resource(std::make_shared<T>(std::forward<R>(resource)))
        , _resource_mutex(std::make_shared<MutexType>()){};
    template <typename... Args>
    explicit lockable_resource(Args &&...args)
        : _resource(std::make_shared<T>(std::forward<Args>(args)...))
        , _resource_mutex(std::make_shared<MutexType>()){};

    /***
     * @brief This function is responsible for calling a callable. Passing the
     * arguments and adding the used resource.
     *
     * C must have as its last argument the type of used resource, otherwise
     * the call will fail and throw.
     *
     * @tparam C Callable type.
     * @tparam Args Argument types to be used with C.
     * @param callable The concrete callable entity of type C.
     * @param args The concrete arguments to be passed to callable.
     * @return Returns what C returns.
     */
    template <typename C, typename... Args>
    constexpr inline auto use(C &&callable, Args &&...args)
    {
        return std::invoke(std::forward<C>(callable),
                           std::forward<Args>(args)..., *_resource);
    }

    template <typename C, typename... Args>
    constexpr inline auto use(C &&callable, Args &&...args) const
    {
        return std::invoke(std::forward<C>(callable),
                           std::forward<Args>(args)..., *_resource);
    }

    template <typename C, typename... Args>
    constexpr inline auto lock_and_use(C &&callable, Args &&...args)
    {
        L lock(*_resource_mutex);
        return std::invoke(std::forward<C>(callable),
                           std::forward<Args>(args)..., *_resource);
    }

    template <typename C, typename... Args>
    constexpr inline auto lock_and_use(C &&callable, Args &&...args) const
    {
        L lock(*_resource_mutex);
        return std::invoke(std::forward<C>(callable),
                           std::forward<Args>(args)..., *_resource);
    }

    friend inline constexpr bool
    operator==(lockable_resource<R, M, L> const &lhs, R const &rhs)
    {
        return *lhs._resource == rhs;
    }

    template <std::convertible_to<T> S, typename K, typename P>
    friend inline constexpr bool
    operator==(lockable_resource<R, M, L> const &lhs,
               lockable_resource<S, K, P> const &rhs)
    {
        return *lhs._resource == *rhs._resource;
    }

    template <typename U>
    bool operator!=(U &&resource)
    {
        return *_resource != std::forward<U>(resource);
    }

    friend inline std::ostream &operator<<(std::ostream &os,
                                           lockable_resource<R, M, L> &lck_res)
    {
        os << *lck_res._resource;
        return os;
    }

private:
    std::shared_ptr<T> _resource;
    std::shared_ptr<MutexType> _resource_mutex;
};

}

#endif //LOCKABLE_RESOURCE_HPP
