/*!
*
* @author spades
* @date 26/02/23
*
*/

#ifndef THREAD_POOL_HPP
#define THREAD_POOL_HPP

#include "include/data_structures/cyclic_constant_size_queue.hpp"
#include "include/type_utilities/type_utilities.hpp"

#include <thread>
#include <condition_variable>
#include <mutex>
#include <iostream>
#include <chrono>
#include <utility>

namespace utilities
{

template <std::size_t NumThreads, utilities::fifo_like_container QueueType,
          utilities::is_execution_request TaskType =
              typename QueueType::value_type>
class thread_pool {
protected:
    using task_queue = QueueType;

    struct thread_guard {
        std::jthread g_thread;
        ~thread_guard()
        {
            try {
                if (g_thread.joinable())
                    g_thread.join();
            } catch (std::system_error &e) {
                std::cout << "Thread " << g_thread.get_id()
                          << " have already stopped." << std::endl;
            }
        }
    };

public:
    using task_type = TaskType;

    thread_pool() = default;

    ~thread_pool()
    {
        stop_all_threads();
    }

    void launch()
    {
        _stop_all_threads = false;

        task_queue &queue = _queue;
        std::condition_variable &signal_variable = _signal_variable;
        bool &stop_all_threads = _stop_all_threads;
        std::mutex &queue_mutex = _queue_mutex;

        auto thread_func = [&queue, &signal_variable, &stop_all_threads,
                            &queue_mutex](std::stop_token const &stoken) {
            while (true) {

                //Beginning of  critical section====================================
                std::unique_lock<std::mutex> lck(queue_mutex);

                while (queue.empty() && !stop_all_threads) {
                    signal_variable.wait(lck);
                }
                if (stop_all_threads) {
                    break;
                }

                TaskType task;

                task = std::move(*queue.begin());
                queue.drop_front();

                lck.unlock();
                //Ending of critical section========================================

                auto exe_cmd = [](auto &exe_cmd) {
                    if constexpr (utilities::smart_pointer<decltype(exe_cmd)>) {
                        if constexpr (utilities::has_run_method<
                                          decltype(exe_cmd)>)
                            std::invoke(
                                &utilities::extract_type_from_smart_pointer_t<
                                    decltype(exe_cmd)>::run,
                                *exe_cmd.get());
                        else if constexpr (utilities::has_execute_method<
                                               decltype(exe_cmd)>)
                            std::invoke(
                                &utilities::extract_type_from_smart_pointer_t<
                                    decltype(exe_cmd)>::execute,
                                *exe_cmd.get());
                        else if constexpr (utilities::is_functor<
                                               decltype(exe_cmd)>)
                            std::invoke(
                                &utilities::extract_type_from_smart_pointer_t<
                                    decltype(exe_cmd)>::operator(),
                                *exe_cmd.get());
                    } else {
                        if constexpr (utilities::has_run_method<
                                          decltype(exe_cmd)>)
                            exe_cmd.run();
                        else if constexpr (utilities::has_execute_method<
                                               decltype(exe_cmd)>)
                            exe_cmd.execute();
                        else if constexpr (utilities::is_functor<
                                               decltype(exe_cmd)>)
                            exe_cmd();
                    }
                };

                if constexpr (utilities::is_variant_v<TaskType>) {
                    std::visit(exe_cmd, task);
                } else {
                    exe_cmd(task);
                }


            }
        };

        if (!active) {
            for (std::size_t i = 0; i < NumThreads; ++i) {
                _threads.emplace_back(thread_func);
                _threads.back().detach();
            }
            active = true;
        }
    }

    template <typename T>
    void insert_task(T &&task)
    {
        std::unique_lock<std::mutex> lck(_queue_mutex);
        _queue.push(std::forward<T>(task));
        _signal_variable.notify_all();
    }

    constexpr std::size_t get_task_queue_size()
    {
        return _queue.size();
    }
    constexpr std::size_t get_num_threads()
    {
        return NumThreads;
    }
    void stop_all_threads()
    {
        using namespace std::chrono_literals;

        std::unique_lock<std::mutex> lck(_queue_mutex);
        _stop_all_threads = true;
        _signal_variable.notify_all();

        for (auto & thrd : _threads){
            if (thrd.joinable()) {
                thrd.join();
            }
        }

        _threads.clear();

        active = false;
    }

private:
    task_queue _queue;
    std::condition_variable _signal_variable;
    std::mutex _queue_mutex;
    bool _stop_all_threads = false;
    bool active = false;
    std::vector<std::jthread> _threads;
};
}
#endif //THREAD_POOL_HPP
