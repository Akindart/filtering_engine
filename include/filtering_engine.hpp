/*!
*
* @author spades
* @date 07/06/23
*
*/

#ifndef FILTERING_ENGINE_HPP
#define FILTERING_ENGINE_HPP

#include "include/type_utilities/type_utilities.hpp"
#include "include/utilities/thread_pool.hpp"
#include "include/utilities/command.hpp"

#include <variant>
#include <memory>
#include <optional>

namespace filtering_engine
{

template <typename... Args>
using prediction_input_list = std::variant<Args...>;

template <typename... Args>
using correction_input_list = std::variant<Args...>;

template <typename State, typename... Targets>
class state_update_handler {
    using _targets_list_ptr = std::tuple<Targets *...>;
    using _targets_list = std::tuple<Targets...>;

public:
    state_update_handler() = default;

    template <typename... Ts>
        requires(sizeof...(Ts) > 0) && (std::is_same_v<Ts, Targets> && ...)
    explicit state_update_handler(Ts *...thread_pools)
        : _targets({ thread_pools... }){};

    template <typename... InfoList>
    void update_state(std::shared_ptr<State> &state, InfoList... info_list)
    {
        throw std::invalid_argument{
            "Information type not handled in prediction step."
        };
    }

    template <typename ThreadPool>
    void add_pool(ThreadPool *tp, std::size_t pos)
    {
        _targets.at(pos) = tp;
    }

    inline constexpr std::size_t qty_thread_pools()
    {
        return sizeof...(Targets);
    }

    inline constexpr bool has_thread_pools()
    {
        return qty_thread_pools() > 0;
    }

    friend std::ostream &operator<<(std::ostream &os,
                                    state_update_handler<State, Targets...> obj)
    {
        os << "state_update_handler";
        return os;
    }

    template <std::size_t idx, typename... InputList>
        requires(sizeof...(Targets) > 0 && idx < sizeof...(Targets))
    void forward_command(std::shared_ptr<State> &state,
                         InputList &&...input_args)
    {
        using target_duo_ptr_t = std::tuple_element_t<idx, _targets_list_ptr>;
        using target_duo_t = std::tuple_element_t<idx, _targets_list>;
        auto &target_duo_var = _targets.at(idx);

        if (const target_duo_ptr_t *target_duo_ptr =
                std::get_if<target_duo_ptr_t>(&target_duo_var)) {
            (*target_duo_ptr)
                ->get_thread_pool()
                ->insert_task(utilities::command_factory::create_unique_cmd(
                    &target_duo_t::target_state_update_t::template update_state<
                        InputList &&...>,
                    *(*target_duo_ptr)->get_state_update(), state,
                    std::forward<InputList>(input_args)...));
        }
    }

private:
    using _thread_pools_list_t = std::variant<Targets *...>;

    std::array<_thread_pools_list_t, sizeof...(Targets)> _targets;
};

template <typename TargetThreadPool,
          typename TargetStateUpdate = std::false_type>
class target_duo {
public:
    using target_state_update_t = TargetStateUpdate;
    using target_thread_pool_t = TargetThreadPool;

    target_duo() = default;

    target_duo(std::shared_ptr<TargetThreadPool> target_thread_pool,
               std::shared_ptr<TargetStateUpdate> target_state_update)
        : _target_thread_pool(target_thread_pool)
        , _target_state_update(target_state_update){};

    auto get_thread_pool()
    {
        return _target_thread_pool;
    }

    auto get_state_update()
    {
        return _target_state_update;
    }

private:
    std::shared_ptr<TargetThreadPool> _target_thread_pool;
    std::shared_ptr<TargetStateUpdate> _target_state_update;
};

template <typename State, utilities::variant_type PredictionInputList,
          utilities::variant_type CorrectionInputList,
          size_t NumThreadsPred = 1, size_t BufferSizePred = 10,
          size_t NumThreadsCor = 1, size_t BufferSizeCor = 10>
    requires(NumThreadsPred >= 1) && (NumThreadsCor >= 1) &&
            (BufferSizePred >= 1) && (BufferSizeCor >= 1) &&
            (std::is_constructible_v<State> &&
             std::is_constructible_v<State, State &> &&
             std::is_move_constructible_v<State>)
class filtering_engine {
protected:
    /*
     * Specialized type traits to automatically generate prediction and correction
     * thread pools
     */

    template <typename S, typename E, typename T, size_t, size_t>
    struct _thread_pool_creation : std::false_type {};

    template <typename StateType, typename StateHandler, typename... Args,
              size_t NumThreads, size_t BufferSize>
              requires (sizeof...(Args) == 1) /// This case expects update_state to receive only one argument in Args
    struct _thread_pool_creation<StateType, StateHandler, std::variant<Args...>,
                                 NumThreads, BufferSize> : std::true_type {
        using qt = data_structures::cyclic_constant_size_queue<
            std::variant<typename utilities::command_factory::command_type<
                             decltype(&StateHandler::template update_state<
                                      Args>)>::unq_ptr...,
                         typename utilities::command_factory::command_type<
                             decltype(&StateHandler::template update_state<
                                      Args &>)>::unq_ptr...,
                         typename utilities::command_factory::command_type<
                             decltype(&StateHandler::template update_state<
                                      Args &&>)>::unq_ptr...,
                         typename utilities::command_factory::command_type<
                             decltype(&StateHandler::template update_state<
                                      Args const>)>::unq_ptr...,
                         typename utilities::command_factory::command_type<
                             decltype(&StateHandler::template update_state<
                                      Args const &>)>::unq_ptr...,
                         typename utilities::command_factory::command_type<
                             decltype(&StateHandler::template update_state<
                                      Args const &&>)>::unq_ptr...>,
            BufferSize>;
        using type = utilities::thread_pool<NumThreads, qt>;
    };

    template <typename StateHandler, typename InputList, size_t NumThreads = 1,
              size_t BufferSize = 1>
    using _thread_pool_type =
        typename _thread_pool_creation<State, StateHandler, InputList,
                                       NumThreads, BufferSize>::type;

    using _state_update_handler_correction = state_update_handler<State>;

    using _correction_thread_pool_type =
        _thread_pool_type<_state_update_handler_correction, CorrectionInputList,
                          NumThreadsCor, BufferSizeCor>;

    using _prediction_target_duo = target_duo<_correction_thread_pool_type,
                                              _state_update_handler_correction>;

    using _state_update_handler_prediction =
        state_update_handler<State, _prediction_target_duo>;

    using _prediction_thread_pool_type =
        _thread_pool_type<_state_update_handler_prediction, PredictionInputList,
                          NumThreadsPred, BufferSizePred>;

    //==========================================================================

private:
    std::shared_ptr<State> _state;

public:
    using state_update_handler_correction = _state_update_handler_correction;
    using state_update_handler_prediction = _state_update_handler_prediction;

    filtering_engine()
        : _state(std::make_shared<State>())
        , _correction_handler_ptr(
              std::make_shared<state_update_handler_correction>())
        , _correction_thread_pool_ptr(
              std::make_shared<_correction_thread_pool_type>())
        , _prediction_handler_ptr(
              std::make_shared<state_update_handler_prediction>(
                  &_pred_target_duo))
        , _prediction_thread_pool_ptr(
              std::make_shared<_prediction_thread_pool_type>())
        , _pred_target_duo()
    {
        _pred_target_duo = _prediction_target_duo(_correction_thread_pool_ptr,
                                                  _correction_handler_ptr);
    };
    explicit filtering_engine(State &state)
        : _state(std::make_shared<State>(state))
        , _correction_handler_ptr(
              std::make_shared<state_update_handler_correction>())
        , _correction_thread_pool_ptr(
              std::make_shared<_correction_thread_pool_type>())
        , _prediction_handler_ptr(
              std::make_shared<state_update_handler_prediction>(
                  &_pred_target_duo))
        , _prediction_thread_pool_ptr(
              std::make_shared<_prediction_thread_pool_type>())
        , _pred_target_duo()
    {
        _pred_target_duo = _prediction_target_duo(_correction_thread_pool_ptr,
                                                  _correction_handler_ptr);
    };
    explicit filtering_engine(State &&state)
        : _state(std::make_shared<State>(state))
        , _correction_handler_ptr(
              std::make_shared<state_update_handler_correction>())
        , _correction_thread_pool_ptr(
              std::make_shared<_correction_thread_pool_type>())
        , _prediction_handler_ptr(
              std::make_shared<state_update_handler_prediction>(
                  &_pred_target_duo))
        , _prediction_thread_pool_ptr(
              std::make_shared<_prediction_thread_pool_type>())
        , _pred_target_duo()
    {
        _pred_target_duo = _prediction_target_duo(_correction_thread_pool_ptr,
                                                  _correction_handler_ptr);
    };

    void start()
    {
        _prediction_thread_pool_ptr->launch();
        _correction_thread_pool_ptr->launch();
    }

    void stop()
    {
        _prediction_thread_pool_ptr->stop_all_threads();
        _correction_thread_pool_ptr->stop_all_threads();
    }

    std::shared_ptr<State> const &get_state() const
    {
        return _state;
    }

    template <typename... Args>
    void reset_state(Args &&...args)
    {
        stop();
        _state->reset(args...);
        start();
    }

    template <std::convertible_to<PredictionInputList>... InputList>
    void input(InputList &&...input_args)
    {
        _prediction_thread_pool_ptr->insert_task(
            utilities::command_factory::create_unique_cmd(
                &state_update_handler_prediction::template update_state<
                    InputList &&...>,
                *_prediction_handler_ptr, _state,
                std::forward<InputList>(input_args)...));

        std::cout << "prediction queue size "
            << _prediction_thread_pool_ptr->get_task_queue_size()
            << std::endl;

    }

    template <std::convertible_to<CorrectionInputList>... InputList>
    void hot_wire_to_correction(InputList &&...input)
    {
        _correction_thread_pool_ptr->insert_task(
            utilities::command_factory::create_unique_cmd(
                &state_update_handler_correction ::template update_state<
                    InputList &&...>,
                *_correction_handler_ptr, _state,
                std::forward<InputList>(input)...));
    }

    bool is_processing_queue_empty()
    {
        bool is_pred_empty = _prediction_thread_pool_ptr->get_task_queue_size() == 0;
        bool is_corr_empty = _correction_thread_pool_ptr->get_task_queue_size() == 0;

        return is_pred_empty && is_corr_empty;
    }

protected:
    _prediction_target_duo _pred_target_duo;

    std::shared_ptr<_prediction_thread_pool_type> _prediction_thread_pool_ptr;
    std::shared_ptr<_correction_thread_pool_type> _correction_thread_pool_ptr;

    std::shared_ptr<_state_update_handler_prediction> _prediction_handler_ptr;
    std::shared_ptr<_state_update_handler_correction> _correction_handler_ptr;
};

}

#endif //FILTERING_ENGINE_HPP
